use slog::Logger;
use avarice::objective::*;
use avarice::errors::Result as AvaResult;
use petgraph::prelude::*;
use rand_mersenne_twister::MTRng;
use rand::Rng;
use std::sync::Mutex;
use setlike::Setlike;
use serde_json;

type Graph = ::petgraph::graph::Graph<f64, f32>;

pub struct Socialbot {
    graph: Graph,
    edge_scale: f64,
    acceptance: Accept,
    bot_index: NodeIndex,
    degree_incentive: bool,
    rng: Mutex<MTRng>,
    /// Run with the WI delta function
    wi: bool,
    log: Logger,
    fof_scale: f64,
}

#[derive(Clone, Default)]
pub struct SocialbotState {
    pub(crate) accepted: Set<NodeIndex>,
    pub(crate) failed: Set<NodeIndex>,
    pub(crate) present: Set<EdgeIndex>,
    pub(crate) absent: Set<EdgeIndex>,
    pub(crate) fofs: Set<NodeIndex>,
}

impl Socialbot {
    pub fn new(rng: MTRng, graph: Graph, bot_index: NodeIndex, degree_incentive: bool, wi: bool, fof_scale: f64, acceptance: Accept, log: Logger) -> Self {
        let edge_scale = graph.node_indices()
            .map(|u| graph.edges_directed(u, Outgoing).map(|e| *e.weight()).sum::<f32>())
            .fold(0.0, |prev, x| if prev < x { x } else { prev }) as f64;

        Socialbot {
            graph, bot_index, acceptance, edge_scale, degree_incentive, wi, log, fof_scale,
            rng: Mutex::new(rng),
        }
    }

    pub fn benefit_breakdown(&self, state: &SocialbotState) -> (f64, f64, f64) {
        let friend_benefit = state.accepted.iter().map(|&u| self.bf(u)).sum::<f64>();
        let edge_benefit = state.present.iter().map(|&e| self.be(e)).sum::<f64>() +
                           state.absent.iter().map(|&e| self.be(e)).sum::<f64>();

        let fof_benefit = state.fofs.iter().map(|&u| self.bfof(u)).sum::<f64>();

        (friend_benefit, edge_benefit, fof_benefit)
    }

    fn accept(&self, u: NodeIndex, state: &SocialbotState) -> f64 {
        let base = self.acceptance.prob(&self.graph, u, state, self.bot_index);
        if self.degree_incentive {
            base + (self.graph.edges_directed(u, Outgoing).map(|er| *er.weight()).sum::<f32>() as f64 / self.edge_scale).powi(5)
        } else {
            base
        }
    }

    fn delta_accept(&self, u: NodeIndex, v: NodeIndex, state: &SocialbotState) -> f64 {
        self.acceptance.delta_prob(&self.graph, u, v, state)
    }

    fn bf(&self, u: NodeIndex) -> f64 {
        self.graph[u]
    }

    fn bfof(&self, u: NodeIndex) -> f64 {
        self.fof_scale * self.graph[u]
    }

    fn be(&self, e: EdgeIndex) -> f64 {
        let (u, v) = self.graph.edge_endpoints(e).unwrap();
        let mut benefit = 1.0 / self.edge_scale;

        if self.graph[u] > 0.0 {
            benefit *= 2.0;
        }
        if self.graph[v] > 0.0 {
            benefit *= 2.0;
        }
        benefit
    }

    /// returns true if the attempt to befriend `u` succeeds
    fn attempt_befriend(&self, u: NodeIndex, state: &SocialbotState) -> bool {
        self.accept(u, state) >= self.rng.lock().unwrap().gen_range(0.0, 1.0)
    }

    fn observe(&self, u: NodeIndex, state: &mut SocialbotState) -> AvaResult<()> {
        let ref mut rng = self.rng.lock().unwrap();
        for er in self.graph.edges_directed(u, Outgoing) {
            let w = er.weight();
            if *w >= rng.gen_range(0.0, 1.0) {
                state.present.insert(er.id());
                if er.target() != u && !state.accepted.contains(&er.target()) {
                    state.fofs.insert(er.target());
                }
            } else {
                state.absent.insert(er.id());
            }
        }
        Ok(())
    }

    fn log_attempt(&self, u: NodeIndex, state: &SocialbotState, successful: bool) {
        let preds = if state.fofs.contains(&u) {
            self.graph.neighbors_directed(u, Incoming).filter(|v| state.accepted.contains(v) || state.failed.contains(v))
                .map(|v| v.index()).collect()
        } else {
            vec![]
        };
        info!(self.log, "attempted insertion"; 
              "weight" => self.bf(u),
              "accept prob" => self.accept(u, state),
              "base accept prob" => self.accept(u, &SocialbotState::default()),
              "target" => u.index(),
              "predecessors" => serde_json::to_string(&preds).unwrap(),
              "successful" => successful);
    }

    fn log_current_benefit(&self, state: &SocialbotState) {
        let (friend, edge, fof) = self.benefit_breakdown(state);
        info!(self.log, "current benefit"; 
              "after step" => state.accepted.len() + state.failed.len(),
              "friend" => friend,
              "edge" => edge,
              "fof" => fof,
              "total" => friend + edge + fof);
    }

    fn delta_wsdm(&self, u: NodeIndex, state: &SocialbotState) -> f64 {
        let mut bs = self.bf(u) + self.graph.edges_directed(u, Outgoing).map(|er| self.be(er.id())).sum::<f64>();
        for edge_ref in self.graph.edges_directed(u, Outgoing) {
            let n = edge_ref.target();
            let w = *edge_ref.weight() as f64;
            if !state.accepted.contains(&n) && !state.fofs.contains(&n) {
                bs += w * self.bfof(n);
            }
        }
        self.accept(u, state) * bs
    }

    fn delta_wi(&self, u: NodeIndex, state: &SocialbotState) -> f64 {
        let p1 = if self.graph.neighbors_directed(u, Outgoing).count() == 0 {
            0.0
        } else {
            1.0 / self.graph.neighbors_directed(u, Outgoing).count() as f64
        };

        let mut p1_sum = 0.0;
        let mut bi_sum = 0.0;
        let mut be_sum = 0.0;

        for edge_ref in self.graph.edges_directed(u, Outgoing) {
            let neighbor = edge_ref.target();
            let weight = *edge_ref.weight() as f64;
            p1_sum += weight * self.delta_accept(u, neighbor, state) * (2.0 * self.bf(neighbor) + self.be(edge_ref.id()));
            if !state.accepted.contains(&neighbor) && !state.fofs.contains(&neighbor) {
                bi_sum += self.bfof(neighbor) * weight;
            }
            be_sum += self.be(edge_ref.id());
        }

        let p1 = p1 * p1_sum;
        let mut p2 = self.bf(u) + bi_sum + be_sum;
        if state.fofs.contains(&u) {
            p2 -= self.bfof(u);
        }
        self.accept(u, state) * (p1 + p2)
    }
}

impl Objective for Socialbot {
    type Element = NodeIndex;
    type State = SocialbotState;

    fn elements(&self) -> ElementIterator<Self> {
        Box::new(self.graph.node_indices())
    }

    // note that this reports **true** benefit, not **expected** benefit
    fn benefit<S: Setlike<Self::Element>>(&self, _s: &S, state: &Self::State) -> AvaResult<f64> {
        let (friend_benefit, edge_benefit, fof_benefit) = self.benefit_breakdown(state);
        Ok(friend_benefit + edge_benefit + fof_benefit)
    }

    // **expected** marginal gain of adding `u` to `S`.
    fn delta<S: Setlike<Self::Element>>(&self, u: NodeIndex, _s: &S, state: &Self::State) -> AvaResult<f64> {
        if self.wi {
            Ok(self.delta_wi(u, state))
        } else {
            Ok(self.delta_wsdm(u, state))
        }
    }

    fn depends(&self, u: NodeIndex, _state: &Self::State) -> AvaResult<ElementIterator<Self>> {
        Ok(Box::new(self.graph
            .neighbors_undirected(u)
            .chain(self.graph
                .neighbors_undirected(u)
                .flat_map(move |v| self.graph.neighbors_directed(v, Outgoing)))
            .filter(move |&v| v != u)))
    }

    fn insert_mut(&self, u: NodeIndex, state: &mut Self::State) -> AvaResult<()> {
        let succ = self.attempt_befriend(u, state);
        self.log_attempt(u, state, succ);
        if succ {
            state.accepted.insert(u);
            state.fofs.remove(&u);
            self.observe(u, state)?;
        } else {
            state.failed.insert(u);
        }
        self.log_current_benefit(state);
        Ok(())
    }
}

#[derive(PartialEq, Debug)]
pub enum Accept {
    ETC,
    ETCZ { zeta: f64, p: f64 },
    HM {
        fin: Vec<Feat>,
        fout: Vec<Feat>,
        zeta: f64,
    },
    HMNM { fin: Vec<Feat>, fout: Vec<Feat> },
}

pub type Feat = Vec<f64>;

const RHO_1: f64 = 0.22605837;
const RHO_0: f64 = 0.18014571;
impl Accept {
    pub fn prob(&self, graph: &Graph, u: NodeIndex, state: &SocialbotState, s: NodeIndex) -> f64 {
        use self::Accept::*;
        match self {
            &ETC => Accept::prob_etc(graph, u, state),
            &ETCZ { zeta, p }=> Accept::prob_etcz(graph, u, state, zeta, p),
            &HM { ref fin, ref fout, zeta } => Accept::prob_hm(fin, fout, zeta, graph, u, state, s),
            &HMNM { .. } => unimplemented!(),
        }
    }

    pub fn delta_prob(&self, graph: &Graph, u: NodeIndex, v: NodeIndex, state: &SocialbotState) -> f64 {
        assert_eq!(self, &Accept::ETC);
        if let Some(e) = graph.find_edge(u, v) {
            RHO_1 * (1.0 + graph[e] as f64 / (Accept::weighted_intersection_size(graph, u, state) + 1.0)).ln()
        } else {
            // no edge u,v, so no change in acceptance
            0.0
        }
    }

    fn weighted_intersection_size(graph: &Graph, u: NodeIndex, state: &SocialbotState) -> f64 {
        graph.edges_directed(u, Outgoing)
            .filter_map(|er| if state.accepted.contains(&er.target()) {
                Some(*er.weight() as f64)
            } else {
                None
            })
        .sum::<f64>()
    }

    fn prob_etc(graph: &Graph, u: NodeIndex, state: &SocialbotState) -> f64 {
        RHO_1 * (Accept::weighted_intersection_size(graph, u, state) + 1.0).ln() + RHO_0
    }

    fn prob_etcz(graph: &Graph, u: NodeIndex, state: &SocialbotState, zeta: f64, p: f64) -> f64 {
        RHO_1 *
        (zeta * Accept::weighted_intersection_size(graph, u, state) + (1.0 - zeta) * p + 1.0)
            .ln() + RHO_0
    }

    fn prob_hm(fin: &Vec<Feat>,
               fout: &Vec<Feat>,
               zeta: f64,
               graph: &Graph,
               u: NodeIndex,
               state: &SocialbotState,
               s: NodeIndex)
               -> f64 {
        let ref fu = fout[u.index()];
        let ref fs = fin[s.index()];
        RHO_1 *
        (dot(fu,
             &graph.neighbors_directed(u, Outgoing)
                 .filter(|node| state.accepted.contains(node))
                 .fold(scale(&fs, 1.0 - zeta),
                       |prev, node| add(&prev, &scale(&fin[node.index()], zeta)))) + 1.0)
            .ln() + RHO_0
    }
}

pub fn dot(a: &Feat, b: &Feat) -> f64 {
    a.into_iter().zip(b.into_iter()).map(|(ai, bi)| ai * bi).sum()
}

pub fn add(a: &Feat, b: &Feat) -> Feat {
    a.into_iter().zip(b.into_iter()).map(|(ai, bi)| ai + bi).collect()
}

pub fn scale(a: &Feat, scale: f64) -> Feat {
    a.into_iter().map(|ai| ai * scale).collect()
}

pub fn unit(a: &Feat) -> Feat {
    scale(a, 1.0 / dot(a, a).sqrt())
}
