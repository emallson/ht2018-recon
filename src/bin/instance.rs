extern crate petgraph;
#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate slog_async;
extern crate capngraph;
extern crate avarice;
#[macro_use]
extern crate avarice_derive;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate bincode;
extern crate docopt;
extern crate slog_json;
extern crate rand_mersenne_twister;
extern crate rand;

use std::collections::HashSet;
use std::io::{BufRead, BufReader};
use std::fs::File;
use rand_mersenne_twister::mersenne;
use rand::Rng;
use petgraph::prelude::*;
use avarice::objective::Set;
use petgraph::visit::{Walker, Bfs};

#[cfg_attr(rustfmt, rustfmt_skip)]
const USAGE: &str = "
Generate a problem setting for the recon simulation.

Usage:
    instance <graph> <out> (--scalar-weights | --features <r>) (--untargeted | --targeted-bfs <t> | --targeted-file <f>) [options]
    instance sample-weights <inst> <count>
    instance total-weight <graph> <inst>
    instance (-h | --help)

Options:
    --untargeted                Benefit is untargeted.
    --targeted-bfs <t>          Benefit targets a random set of <t> BFS targets.
    --targeted-file <f>         Targets are listed in file <f>.
    --scalar-weights            Edge weights are scalar. Used for ETC model.
    --features <r>              Nodes are given unit <r>-vector determining their features and edge weights.
                                These vectors are chosen uniformly on the r-1-dimensional unit sphere, then 
                                half the points are reflected so that all dot products are positive.
";

type Feat = Vec<f64>;

#[derive(Serialize, Deserialize)]
struct Args {
    arg_graph: Option<String>,
    arg_out: Option<String>,
    flag_untargeted: bool,
    flag_targeted_bfs: Option<usize>,
    flag_targeted_file: Option<String>,
    flag_scalar_weights: bool,
    flag_features: Option<usize>,
    cmd_sample_weights: bool,
    cmd_total_weight: bool,
    arg_inst: Option<String>,
    arg_count: Option<usize>,
}

fn load_graph(args: &Args, path: &str) -> Graph<f64, f32> {
    let mut rng = mersenne();
    let g = capngraph::load_graph(path).unwrap();

    if args.flag_untargeted {
        g.map(|_, _| rng.gen_range(0.0, 1.0), |_, &w| w)
    } else if let Some(size) = args.flag_targeted_bfs {
        let u = rand::sample(&mut rng, g.node_indices(), 1)[0];
        let targets = Bfs::new(&g, u).iter(&g).take(size).collect::<Set<_>>();
        g.map(|node, _| if targets.contains(&node) { 1.0 } else { 0.0 },
              |_, &w| w)
    } else if let Some(ref path) = args.flag_targeted_file {
        let f = File::open(path).unwrap();
        let file = BufReader::new(&f);
        let targets = file.lines().map(|s| NodeIndex::new(s.unwrap().parse::<usize>().unwrap())).collect::<Set<NodeIndex>>();
        g.map(|node, _| if targets.contains(&node) { 1.0 } else { 0.0 },
              |_, &w| w)
    } else {
        panic!()
    }
}

fn reweight_uniform(g: &mut Graph<f64, f32>) {
    let mut rng = mersenne();
    for edge in g.edge_indices() {
        g[edge] = rng.gen_range(0.0, 1.0);
    }
}

fn dot(a: &Feat, b: &Feat) -> f64 {
    a.into_iter().zip(b.into_iter()).map(|(ai, bi)| ai * bi).sum()
}

fn scale(a: &Feat, scale: f64) -> Feat {
    a.into_iter().map(|ai| ai * scale).collect()
}

fn unit(a: &Feat) -> Feat {
    scale(a, 1.0 / dot(a, a).sqrt())
}

// output is (fin, fout)
fn generate_features(n: usize, r: usize) -> (Vec<Feat>, Vec<Feat>) {
    let mut rng = mersenne();
    use rand::distributions::{IndependentSample, Normal};
    let norm = Normal::new(0.0, 1.0);
    let mut feats = vec![];
    for _ in 0..2 * n {
        let mut feat = vec![0.0; r];
        for i in 0..r {
            feat[i] = norm.ind_sample(&mut rng).abs();
        }
        feat = unit(&feat);
        assert!((dot(&feat, &feat).sqrt() - 1.0).abs() <= 1e-6);
        feats.push(feat);
    }

    let fout = feats.split_off(n);
    (feats, fout)
}

#[derive(Serialize, Deserialize)]
enum Instance {
    Scalar {
        edge_weights: Vec<f32>,
        node_weights: Vec<f64>,
    },
    Vector {
        r: usize,
        fin: Vec<Feat>,
        fout: Vec<Feat>,
        node_weights: Vec<f64>,
    },
}

fn total_weight(g: Graph<(), f32>, weights: Vec<f64>, edge_weights: Vec<f32>) -> f64 {
    let edge_scale = g.node_indices()
        .map(|u| g.edges_directed(u, Outgoing).map(|e| edge_weights[e.id().index()]).sum::<f32>())
        .fold(0.0, |prev, x| if prev < x { x } else { prev }) as f64;

    let edge_weight = g.edge_references().map(|er| {
        let u = er.source();
        let v = er.target();
        let mut b = 1.0 / edge_scale;

        if weights[u.index()] > 0.0 {
            b *= 2.0;
        }
        if weights[v.index()] > 0.0 {
            b *= 2.0;
        }

        b
    }).sum::<f64>();
    let node_weight = weights.into_iter().sum::<f64>();

    node_weight + edge_weight
}

fn main() {
    let args: Args = docopt::Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    if args.cmd_total_weight {
        let g = capngraph::load_graph(&args.arg_graph.unwrap()).unwrap();
        let inst = bincode::deserialize_from(&mut File::open(args.arg_inst.as_ref().unwrap()).unwrap(), bincode::Infinite).unwrap();
        match inst {
            Instance::Scalar { node_weights, edge_weights } => {
                println!("{}", total_weight(g, node_weights, edge_weights));
            },
            Instance::Vector { .. } => {
                panic!("not implemented for vector case")
            }
        }
    } else if args.cmd_sample_weights {
        let inst = bincode::deserialize_from(&mut File::open(args.arg_inst.as_ref().unwrap()).unwrap(), bincode::Infinite).unwrap();
        match inst {
            Instance::Scalar { edge_weights, .. } => {
                println!("{}", serde_json::to_string(&rand::sample(&mut mersenne(), &edge_weights, args.arg_count.unwrap())).unwrap());
            },
            Instance::Vector { fin, fout, .. } => {
                assert_eq!(fin.len(), fout.len());
                let mut rng = mersenne();
                let mut res = vec![];
                let mut covered: HashSet<(usize, usize)> = HashSet::default();
                for _ in 0..args.arg_count.unwrap() {
                    loop {
                        let u = rng.gen_range(0, fin.len());
                        let v = rng.gen_range(0, fout.len());

                        if covered.contains(&(u, v)) {
                            continue;
                        }

                        covered.insert((u, v));
                        res.push(dot(&fin[u], &fout[v]));
                        break;
                    }
                }

                println!("{}", serde_json::to_string(&res).unwrap());
            }
        }
    } else {
        let mut g = load_graph(&args, args.arg_graph.as_ref().unwrap());
        g.add_node(0.0);

        let node_weights = g.node_indices().map(|u| g[u]).collect();

        let inst = if args.flag_scalar_weights {
            reweight_uniform(&mut g);
            Instance::Scalar {
                node_weights,
                edge_weights: g.edge_references().map(|er| *er.weight()).collect()
            }
        } else if let Some(r) = args.flag_features {
            let (fin, fout) = generate_features(g.node_count(), r);
            Instance::Vector {
                node_weights, r, fin, fout
            }
        } else {
            unreachable!()
        };
        bincode::serialize_into(&mut File::create(args.arg_out.as_ref().unwrap()).unwrap(), &inst, bincode::Infinite).unwrap();
    }
}
