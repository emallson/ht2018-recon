extern crate petgraph;
#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate slog_async;
extern crate capngraph;
extern crate setlike;
extern crate avarice;
#[macro_use]
extern crate avarice_derive;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate bincode;
extern crate docopt;
extern crate slog_json;
extern crate rand_mersenne_twister;
extern crate rand;

mod objective;

use objective::*;

use slog::{Drain, Logger};
use petgraph::prelude::*;
use std::fs::File;
use rand_mersenne_twister::mersenne;
use rand::Rng;
use avarice::objective::*;
use avarice::greedy::greedy;

#[cfg_attr(rustfmt, rustfmt_skip)]
const USAGE: &str = "
Recon attack simulation

Usage:
    recon <graph> <inst> <k> (--etc | --hmnm | --zeta <zeta> | --etc-zeta <zeta>)[options]
    recon (-h | --help)

Options:
    -h --help                   Show this screen.
    --etc                       Expected triadic closure acceptance.
    --etc-zeta <zeta>           Expected triadic closure acceptance with ζ.
    --zeta <zeta>               HM + ζ acceptance.
    --hmnm                      Non-Monotone HM acceptance.
    --degree-incentive          Enable degree incentive in acceptance function.
    --wi                        Use the WI delta function.
    --fof-scale <scale>         Set B_fof(u) = <scale> B_f(u). [default: 0.5]
    --log <log>                 Log to write output to.
";

#[derive(Serialize, Deserialize)]
struct Args {
    arg_graph: String,
    arg_inst: String,
    arg_k: usize,
    flag_etc: bool,
    flag_etc_zeta: Option<f64>,
    flag_hmnm: bool,
    flag_zeta: Option<f64>,
    flag_degree_incentive: bool,
    flag_wi: bool,
    flag_log: Option<String>,
    flag_fof_scale: f64,
}

fn reweight_homophily(g: &mut Graph<f64, f32>, fin: &Vec<Feat>, fout: &Vec<Feat>) {
    for edge in g.edge_indices() {
        let (u, v) = g.edge_endpoints(edge).unwrap();
        g[edge] = dot(&fout[u.index()], &fin[v.index()]) as f32;
    }
}

#[derive(Serialize, Deserialize)]
enum Instance {
    Scalar {
        edge_weights: Vec<f32>,
        node_weights: Vec<f64>,
    },
    Vector {
        r: usize,
        fin: Vec<Feat>,
        fout: Vec<Feat>,
        node_weights: Vec<f64>,
    },
}

fn load_inst(path: &str) -> Instance {
    bincode::deserialize_from(&mut File::open(path).unwrap(), bincode::Infinite).unwrap()
}

fn apply_inst(g: Graph<(), f32>, inst: &Instance) -> Graph<f64, f32> {
    match inst {
        &Instance::Scalar { ref node_weights, ref edge_weights } => {
            g.map(|id, _| node_weights[id.index()],
                  |id, _| edge_weights[id.index()])
        },
        &Instance::Vector { ref node_weights, ref fin, ref fout, .. } => {
            let mut gp = g.map(|id, _| node_weights[id.index()],
            |_, &w| w);

            reweight_homophily(&mut gp, fin, fout);
            gp
        }
    }
}

fn main() {
    let args: Args = docopt::Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let log = {
        let term = slog_term::TermDecorator::new().build();
        let term_drain = slog_term::CompactFormat::new(term).build().fuse();
        let term_drain = slog_async::Async::new(term_drain).build().fuse();

        if let Some(ref log) = args.flag_log {
            let file_drain = slog_json::Json::default(File::create(log).unwrap()).fuse();
            let file_drain = slog_async::Async::new(file_drain).build().fuse();
            slog::Logger::root(slog::Duplicate::new(term_drain, file_drain).fuse(),
                               o!("version" => env!("CARGO_PKG_VERSION")))
        } else {
            slog::Logger::root(term_drain, o!("version" => env!("CARGO_PKG_VERSION")))
        }
    };

    info!(log, "parameters"; "args" => serde_json::to_string(&args).unwrap());
    let mut g = capngraph::load_graph(&args.arg_graph).unwrap();
    let bot_index = g.add_node(());
    let inst = load_inst(&args.arg_inst);
    let g = apply_inst(g, &inst);
    info!(log, "loaded graph"; "nodes" => g.node_count(), "edges" => g.edge_count(), "targets" => g.node_indices().filter(|&n| g[n] > 0.0).count());

    let obj_log = log.new(o!("section" => "bot"));
    let di = args.flag_degree_incentive;
    let obj = match inst {
        Instance::Scalar { .. } => {
            if args.flag_etc {
                Socialbot::new(mersenne(), g, bot_index, di, args.flag_wi, args.flag_fof_scale, Accept::ETC, obj_log)
            } else if let Some(zeta) = args.flag_etc_zeta {
                Socialbot::new(mersenne(), g, bot_index, di, args.flag_wi, args.flag_fof_scale, Accept::ETCZ {
                    zeta, p: 0.2
                }, obj_log)
            } else {
                unreachable!()
            }
        }, 
        Instance::Vector { fin, fout, .. } => {
            if args.flag_hmnm {
                Socialbot::new(mersenne(), g, bot_index, di, args.flag_wi, args.flag_fof_scale, Accept::HMNM {
                    fin, fout
                }, obj_log)
            } else if let Some(zeta) = args.flag_zeta {
                Socialbot::new(mersenne(), g, bot_index, di, args.flag_wi, args.flag_fof_scale, Accept::HM {
                    fin, fout, zeta
                }, obj_log)
            } else {
                panic!("instance is feature-based but --etc given")
            }
        },
    };

    info!(log, "constructed objective");

    let (_, sol, state) = greedy(&obj, args.arg_k, Some(log.new(o!("section" => "greedy")))).unwrap();

    fn display_node(nix: &NodeIndex) -> usize {
        nix.index()
    }

    // fn display_edge<N, E>(g: &Graph<N, E>, eix: &EdgeIndex) -> (usize, usize) {
        // let (u, v) = g.edge_endpoints(*eix).unwrap();
        // (u.index(), v.index())
    // }

    let (fb, eb, fofb) = obj.benefit_breakdown(&state);
    info!(log, "objective"; "f" => obj.benefit(&sol.iter().cloned().collect::<Set<_>>(), &state).unwrap(),
          "friend" => fb,
          "edge" => eb,
          "fof" => fofb);
    info!(log, "solution"; 
          "sol" => serde_json::to_string(&sol.iter().map(display_node).collect::<Vec<_>>()).unwrap(),
          "accepted" => serde_json::to_string(&state.accepted.iter().map(display_node).collect::<Vec<_>>()).unwrap(),
          "fofs" => serde_json::to_string(&state.fofs.iter().map(display_node).collect::<Vec<_>>()).unwrap());
}
